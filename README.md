# showcase-app

## Slides:
http://bit.ly/2sNOUT3


## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```



## Azure

Create Docker VM( by docker machine), don't forget to add your subscription id :

Example :

```shell
docker-machine create --driver azure \
                      --azure-subscription-id $AZURE_SUBSCRIPTION_ID \
                      --azure-size Standard_A1 \
                      --azure-location eastus \
                      VM_NAME
```



```shell
docker-machine create --driver azure \
                      --azure-size Standard_A1 \
                      --azure-location eastus \
                      staging-azure-vm

docker-machine create --driver azure \
                      --azure-size Standard_A1 \
                      --azure-location eastus \
                      prod-azure-vm

```



