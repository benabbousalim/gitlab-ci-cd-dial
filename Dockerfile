FROM node:10-alpine

ENV NODE_ENV production

WORKDIR /app

ADD package*.json /app/
RUN npm install --silent

ADD dist /app/

EXPOSE 3000

CMD node main.js
